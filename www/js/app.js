(function(ns,$w){

var app = {
    domElements:{
        $wrap : $('#app-wrap'),
        $messenger : $('#messenger'),
        $stage: $('#stage')
    },
    videoSettings: {
        wrapper: 'ambilight-container',
        selector:'camera-stream',
        $el: null,
        $wrapper: null
    },
    init: function(){
        var t = this;
        t.translateTo('en');
        t.initStage();
        t.initComponents();
    },
    translateTo: function(langCode){
        if(ns.translations[langCode]){
            ns.translation = ns.translations[langCode];
        } else {
            ns.translation = ns.translations['en'];
        }
    },
    initStage: function(){
        var width = ($w.innerWidth()*0.99);
        if(width>500){ width = 500; }
        var height = ($w.innerHeight()*0.99),
        mtop = -(height/2),
        mleft = -(width/2);
        this.domElements.$wrap.css({
            'width': width,
            'height': height,
            'margin-left': mleft,
            'margin-top': mtop
        });
    },
    initComponents: function(){
        var t = this;
        setTimeout(function(){
            t.domElements.$wrap.removeClass('splash');
        },1500);
        setTimeout(function(){
            t.listenToBridgeEvents();
            t.listenToUserEvents();
            t.listenToLightsEvents();
            t.listenToAmbilightsEvents();
            t.hueConnector = ns.connector.init();
        },1700);
    },
    listenToBridgeEvents: function(){
        var t = this,
        $doc = $(document);
        $doc.on("discoverBridgeOnStart", function (evt) {
            t.startLoading();
            var $bridgeLocator = $('#hue-bridgeLocator');
            $bridgeLocator.removeClass('errorState');
            if ($bridgeLocator.length <= 0) {
                t.messenger(ns.translation.hue.bridge.lookingForABridge);
                var bridgeLocator = '<div id="hue-bridgeLocator" class="step">' +
                        '<span class="icon icon-signal step-icon"></span>' +
                        '<span class="loader"></span>'+
                        '</div>';
                t.updateStage(bridgeLocator, function() {
                    $bridgeLocator = $('#hue-bridgeLocator');
                });
            }
        });
        $doc.on("discoverBridgeOnSuccess", function (evt) {
            setTimeout(function(){
                t.stopLoading();
                var data = evt.params,
                onSuccess = data.onSuccess;
                onSuccess && onSuccess();
            },2000);

        });
        $doc.on("discoverBridgeOnError", function (evt) {
            setTimeout(function(){
                var data = evt.params;
                t.stopLoading();
                var $bridgeLocator = $('#hue-bridgeLocator');
                $bridgeLocator.addClass('errorState');
                if ($bridgeLocator.find('#bridge-error').length <= 0) {
                    $bridgeLocator.find('#bridge-error').remove();
                    $bridgeLocator.append('<div id="bridge-error"><span>' + ns.translation.hue.bridge.discoveryFailed + '</span>' + ns.app.createButton(ns.translation.hue.bridge.tryAgain) + '</div>');
                    $bridgeLocator.find('.progress-button button').on('click',function(e){
                        e.preventDefault();
                        data.tries++;
                        t.hueConnector.discoverBridge(data.onSuccess, data.tries);
                    });
                }
            },2000);
        });
    },
    listenToUserEvents: function(){
        var t = this,
        $doc = $(document);
        $doc.on("createUserOnStart", function (evt) {
            var $bridgeLocator = $('#hue-bridgeLocator');
            $bridgeLocator.removeClass('errorState');
            t.startLoading();
        });
        $doc.on("createUserOnSuccess", function (evt) {
            setTimeout(function(){
                var data = evt.params;
                t.stopLoading();
                var cb = data.cb;
                cb && cb();
            },2000);
        });
        $doc.on("createUserOnError", function (evt) {
            setTimeout(function(){
                var data = evt.params,
                error = data.error,
                cb = data.cb;
                t.stopLoading();
                if ($('#createUserError').length <= 0) {
                    t.error('<div id="hue-bridgeLocator" class="step errorState"><span class="icon icon-signal step-icon"></span><span class="loader"></span><div id="createUserError">' + error + ns.app.createButton(ns.translation.hue.bridge.tryAgain) + '</div></div>');
                    setTimeout(function() {
                        $('#createUserError').find('.progress-button button').on('click',function(e){
                           e.preventDefault();
                           t.hueConnector.createUser(cb);
                        });
                    }, 800);
                }
            },2000);
        });
    },
    listenToLightsEvents: function(){
        var t = this,
        $doc = $(document);

        $doc.on("discoverLightsOnStart", function (evt) {
            t.messenger(ns.translation.hue.lights.lookingForLights);
            t.updateStage('<div id="hue-lightsLocator" class="step"><span class="icon icon-bulb step-icon"></span><span class="loader"></span></div>');
            t.startLoading();
        });

        $doc.on("discoverLightsOnSuccess", function (evt) {
            setTimeout(function(){
                t.stopLoading();
                t.createLightsList();
            },2000);
        });

        $doc.on("discoverLightsOnError", function (evt) {
            setTimeout(function(){
                t.stopLoading();
                var data = evt.params;
                t.error(data.error);
            },2000);
        });
    },
    listenToAmbilightsEvents: function(){
        var t = this,
        $doc = $(document);
        $doc.on("checkGetUserMediaSupportError", function (evt) {
            t.error('<div class="step errorState"><span class="icon icon-cross step-icon"></span><div id="lights-error"><p>'+ns.translation.ambilight.getUserMediaNotSupported+'</p></div></div>');
        });
    },
    createLightsList: function(){
        var t = this,
        lights = t.hueConnector.lights,
        i = 1,
        cpt=0,
        lightsList='<div id="lightsList">',
        lightsUl='<ul>';
        for(i in lights){
            lightsUl+='<li><a class="light-selector" href="#" data-light="'+i+'">'+lights[i].name+'<span></span></a></li>';
            cpt++;
        }
        if(cpt==0){
            lightsList+='<div id="hue-lightsLocator" class="step errorState"><span class="icon icon-bulb step-icon"></span><div id="lights-error"><p>'+ns.translation.hue.lights.noLightsFound+'</p><a href="#" class="btn">'+ns.translation.hue.lights.searchForLights+'</a></div></div>';
        } else {
            lightsList+=lightsUl+'</ul>'+
            '<a href="#" class="btn ambilightStarter hidden">'+ns.translation.ambilight.start+'</a>';
        }
        lightsList+='</div>';
        ns.app.messenger(ns.translation.hue.lights.selectYourLights);
        t.updateStage(lightsList,function(){
            if(cpt==0){
                $('#lightsList .btn').on('click',function(e){
                    e.preventDefault();
                    t.hueConnector.searchForLights();
                });
            } else {
                $('#lightsList .light-selector').on('click',function(e){
                    e.preventDefault();
                    var lid = $(this).attr('data-light');
                    if(!$(this).hasClass('selected')){
                        t.hueConnector.lights[lid].ambilight = true;
                        $(this).addClass('selected');
                    } else {
                        t.hueConnector.lights[lid].ambilight = false;
                        $(this).removeClass('selected');
                    }
                    if($('#lightsList .selected').length>0){
                        $('#lightsList .ambilightStarter').removeClass('hidden');
                    } else {
                        $('#lightsList .ambilightStarter').addClass('hidden');
                    }
                });
                $('#lightsList .ambilightStarter').on('click',function(e){
                    e.preventDefault();
                    t.createAmbilightElement(function(){
                        t.videoSettings.$wrapper = document.getElementById(t.videoSettings.wrapper);
                        t.videoSettings.$el = document.getElementById(t.videoSettings.selector);
                        t.ambilight = ns.ambilight.init(t.videoSettings,function(hexColor){
                            $(t.domElements.$wrap).addClass('ambilighting');
                            t.ambilightCb(hexColor);
                        });
                    });
                });
            }
        });
    },
    createAmbilightElement: function(cb){
        var t = this,
        videoElt = document.createElement('div');
        videoElt.id = t.videoSettings.wrapper;
        videoElt.innerHTML = '<div id="video-container">'+
                        '<video id="'+t.videoSettings.selector+'" autoplay width="'+$('#app-top').innerWidth()+'" height="'+$('#app-top').innerHeight()+'"></video>'+
                        '<a href="" class="btn ambilightStopper">'+ns.translation.ambilight.stop+'</a>'+
                    '</div>';
        t.updateStage(videoElt,function(){
            $('#video-container .btn').on('click',function(e){
                e.preventDefault();
                $(t.domElements.$wrap).removeClass('ambilighting');
                t.ambilight.videoSettings.currentlyStreaming = false;
                t.ambilight.stream.stop();
                t.createLightsList();
                setTimeout(function(){
                    t.adjustGradient('FFFFFF');
                },1000);
            });
            cb && cb();
        });
    },
    ambilightCb: function(hexColor){
        var t = this,
        xyb = ns.colorConverter.hexStringToXyBri(hexColor),
        hueColor = ns.colorConverter.xyBriForModel(xyb, 'LCT001');
        t.hueConnector.updateBulbColor(hueColor);
        t.adjustGradient(hexColor);
    },
    adjustGradient: function(hexColor){
        var Gradient = {
            /* IE10 Consumer Preview */
            'background-image': '-ms-radial-gradient(#'+hexColor+' 0%, #FFFFFF 100%)',
            /* Mozilla Firefox */
            'background-image': '-moz-radial-gradient(#'+hexColor+' 0%, #FFFFFF 100%)',
            /* Opera */
            'background-image': '-o-radial-gradient(#'+hexColor+' 0%, #FFFFFF 100%)',
            /* Webkit (Safari/Chrome 10) */
            'background-image': '-webkit-gradient(radial, center center, 0, center center, 497, color-stop(0, #'+hexColor+'), color-stop(1, #FFFFFF))',
            /* Webkit (Chrome 11+) */
            'background-image': '-webkit-radial-gradient(#'+hexColor+' 0%, #FFFFFF 100%)',
            /* W3C Markup, IE10 Release Preview */
            'background-image': 'radial-gradient(#'+hexColor+' 0%, #FFFFFF 100%)'
        };
        $('#app-top').css(Gradient);
    },
    createButton: function(btnTxt){
        var btn = '<div class="progress-button">'+
            '<button><span>'+btnTxt+'</span></button>'+
            //'<svg class="progress-circle" width="70" height="70"><path d="m35,2.5c17.955803,0 32.5,14.544199 32.5,32.5c0,17.955803 -14.544197,32.5 -32.5,32.5c-17.955803,0 -32.5,-14.544197 -32.5,-32.5c0,-17.955801 14.544197,-32.5 32.5,-32.5z"/></svg>'+
            //'<svg class="checkmark" width="70" height="70"><path d="m31.5,46.5l15.3,-23.2"/><path d="m31.5,46.5l-8.5,-7.1"/></svg>'+
            //'<svg class="cross" width="70" height="70"><path d="m35,35l-9.3,-9.3"/><path d="m35,35l9.3,9.3"/><path d="m35,35l-9.3,9.3"/><path d="m35,35l9.3,-9.3"/></svg>'+
        '</div>';
        return btn;
    },
    startLoading: function(){
        this.domElements.$wrap.addClass('loading');
    },
    stopLoading: function(){
        this.domElements.$wrap.removeClass('loading');
    },
    notification: function(code,msg){
        var notif = '<span class="notification '+code+'">'+msg+'</span>';
        this.updateStage(notif);
    },
    messenger: function(msg,cb){
        var t = this;
        t.domElements.$messenger.removeClass('onStage');
        setTimeout(function(){
            t.domElements.$messenger.html(msg).addClass('onStage');
            cb && cb();
        },400);
    },
    updateStage: function(msg,cb){
        var t = this;
        t.domElements.$stage.removeClass('onStage');
        setTimeout(function(){
            t.domElements.$stage.html(msg).addClass('onStage').css({
                width: $('#app-bottom').innerWidth(),
                height: $('#app-bottom').innerHeight()
            });
            cb && cb();
        },800);
    },
    error: function(errmsg){
        this.notification('error',errmsg);
    }
};
ns.app = app;

ns.app.init();

})(hueApp,$(window));