(function(ns){

var translations = {
  en: {
      hue: {
          bridge: {
              lookingForABridge : "Looking for your bridge",
              discoveryFailed: 'We are sorry, but we were unable to contact your bridge.',
              pressLinkBtn:'Please presse the link button and',
              tryAgain: 'Let\'s try Again'
          },
          lights: {
              lookingForLights : "Looking for your lights",
              noLightsFound: 'No lights found',
              searchForLights: 'Search for lights',
              selectYourLights: 'Select the lights to run with ambighue'
          }
      },
      ambilight: {
          missingCallBack: 'Please provide a valid ambilight callback',
          missingVideoElt: 'Please provide a valid element to listen to',
          getUserMediaNotSupported: 'We are sorry, but it seems your browser does not support getUserMedia, which is required to run ambighue',
          stop: 'Stop',
          start: 'Start',
          authorizeCameraAccess: 'Please authorize access to your camera',
          ambilighting: 'Ambilighting'
      }
  }
};
ns.translations = translations;

})(hueApp);