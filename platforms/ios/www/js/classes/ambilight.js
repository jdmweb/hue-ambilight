(function(ns){

var ambilight = {
    userMediaSettings: {
        video: true
    },
    colorThief: new ColorThief(),
    callBack: null,
    stream: null,
    init: function(videoSettings, cb){
        var t = this;
        videoSettings.currentlyStreaming = false;
        t.videoSettings = videoSettings;
        if(!cb){
            t.error(ns.translation.ambilight.missingCallBack);
        }
        if(t.videoSettings.$el===null){
            t.error(ns.translation.ambilight.missingVideoElt);
        }
        if(t.checkGetUserMediaSupport()){
            t.callBack = cb;
            t.videoSettings.$el = document.getElementById(t.videoSettings.selector);
            t.startStreaming();
            t.startSnapshotLoop();
        } else {
            t.triggerEvent('checkGetUserMediaSupportError', {});
        }
        return t;
    },
    checkGetUserMediaSupport: function(){
        if(navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia){
            navigator.getUserMedia  = navigator.getUserMedia ||
                          navigator.webkitGetUserMedia ||
                          navigator.mozGetUserMedia ||
                          navigator.msGetUserMedia;
            return true;
        } else {
            return false;
        }
    },
    startStreaming: function(){
        var t = this;
        ns.app.messenger(ns.translation.ambilight.authorizeCameraAccess);
        navigator.getUserMedia(t.userMediaSettings,function(localMediaStream){
            t.getUserMediaSuccess(localMediaStream);
        },function(errMsg){
            if(errMsg.name=='PermissionDeniedError'){
                t.error(ns.translation.ambilight.getUserMediaNotSupported);
            }
        });
    },
    getUserMediaSuccess: function(localMediaStream){
        var t = this;
        t.stream=localMediaStream;
        ns.app.messenger(ns.translation.ambilight.ambilighting);
        setTimeout(function(){ t.videoSettings.currentlyStreaming = true; }, 4000);
        t.videoSettings.$el.src = window.URL.createObjectURL(t.stream);
    },
    startSnapshotLoop: function(){
        var t = this;
        window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
            window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
            this.takeSnapshot();
    },
    takeSnapshot: function(){
        var t = this;
        if(t.videoSettings.currentlyStreaming===true){
            var t = this;
            t.analyzePic();
        }
        setTimeout(function(){
            t.takeSnapshot();
        },500);
    },
    analyzePic: function(){
        var t = this,
        mainCol = t.colorThief.getColor(t.videoSettings.$el),
        hexColor = t.rgbToHex(mainCol[0],mainCol[1],mainCol[2]);
        t.callBack && t.callBack(hexColor);
    },
    rgbToHex:function(r, g, b) {
        return ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
    },
    error: function(errmsg){
        console.log(errmsg);
        ns.app.error(errmsg);
    },
    triggerEvent: function(name,data){
        data.time = new Date();
        var event = $.Event( name );
        event.params = data;
        $(document).trigger(event);
    },
};

ns.ambilight = ambilight;

})(hueApp);