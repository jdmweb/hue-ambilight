(function(ns) {

    var connector = {
        bridge: {
            internalipaddress: null
        },
        lights: {
        },
        username: ns.simulator.username, //null,//'35804d64c27dd8226834effccccdf',
        init: function() {
            var t = this;
            if (this.bridge.internalipaddress === null) {
                this.discoverBridge(function() {
                    t.searchForLights();
                }, 1);
            } else {
                t.searchForLights();
            }
            return this;
        },
        discoverBridge: function(onSuccess, tries) {
            var t = this,
                discoverUrl = 'http://www.meethue.com/api/nupnp';

            t.triggerEvent('discoverBridgeOnStart', {});

            var req = $.getJSON(discoverUrl, {}, function(discovered) {
                if (ns.simulate) {
                    discovered = ns.simulator.bridge;
                }
                if (discovered && discovered[0] && discovered[0].internalipaddress) {
                    t.bridge = discovered[0];
                    if(t.getUser() === null) {
                        t.createUser(onSuccess);
                    }
                    else {
                        t.triggerEvent('discoverBridgeOnSuccess', {onSuccess: onSuccess});
                    }
                } else {
                    t.triggerEvent('discoverBridgeOnError', {onSuccess: onSuccess, tries: tries});
                }
            });
            req.error(function(xhr, thrownError) {
                t.error(thrownError);
            });
        },
        getUser: function(){
            var t = this;
            if (ns.simulate) {
                t.username = ns.simulator.username;
            } else {
                if(t.supports_html5_storage()){
                    t.username = localStorage.getItem("hue.userName");
                }
            }
            return t.username;
        },
        createUser: function(cb, btnInstance) {
            var t = this;
            t.triggerEvent('createUserOnStart', {});
            $.ajax({
                url: 'http://' + t.bridge.internalipaddress + '/api',
                type: 'POST',
                data: '{"devicetype":"html5"}',
                success: function(data) {
                    setTimeout(function(){
                        if (data[0].success) {
                            t.username = data[0].success.username;
                            if(t.supports_html5_storage()){
                                localStorage.setItem("hue.userName",t.username);
                            }
                            t.triggerEvent('createUserOnSuccess', {cb: cb});
                        } else {
                            var error ='';
                            if (data[0].error) {
                                error = ns.translation.hue.bridge.pressLinkBtn;
                            } else {
                                error = data;
                            }
                            t.triggerEvent('createUserOnError', {error: error,cb:cb});
                        }
                    },1000);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    t.triggerEvent('createUserOnError', {error: textStatus + ', ' + errorThrown,cb:cb});
                }
            });
        },
        searchForLights: function() {
            var t = this;
            if (t.bridge.internalipaddress) {

                t.triggerEvent('discoverLightsOnStart', {});

                $.ajax({
                    url: 'http://' + t.bridge.internalipaddress + '/api/' + t.username + '/lights',
                    type: 'GET',
                    timeout: 5000,
                    success: function(lights) {
                        if (ns.simulate) {
                            t.lights = ns.simulator.lights;
                        } else {
                            t.lights = lights;
                        }
                        t.triggerEvent('discoverLightsOnSuccess', {});
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        if (ns.simulate) {
                            t.lights = ns.simulator.lights;
                            t.triggerEvent('discoverLightsOnSuccess', {});
                        } else {
                            t.triggerEvent('discoverLightsOnError', {error: textStatus + ', ' + errorThrown});
                        }
                    }
                });
            } else {
                t.discoverBridge(function() {
                    t.searchForLights();
                }, 1);
            }
        },
        updateBulbColor: function(newCol) {
            var t = this,
            i = 1;
            for (i in t.lights) {
                if (t.lights[i].ambilight) {
                    $.ajax({
                        url: 'http://' + t.bridge.internalipaddress + '/api/' + t.username + '/lights/' + i + '/state',
                        type: 'PUT',
                        data: '{"on":true,"xy":[' + newCol.x + ',' + newCol.y + ']}'
                    });
                }
            }
        },
        triggerEvent: function(name,data){
            data.time = new Date();
            var event = $.Event( name );
            event.params = data;
            $(document).trigger(event);
        },
        error: function(e){
            ns.app.error(e);
        },
        supports_html5_storage: function() {
          try {
            return 'localStorage' in window && window['localStorage'] !== null;
          } catch (e) {
            return false;
          }
        }
    };
    ns.connector = connector;

})(hueApp);